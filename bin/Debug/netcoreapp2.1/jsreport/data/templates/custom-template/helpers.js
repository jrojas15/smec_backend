function round(value) {
    return Math.round(value * 100) / 100;
}

function dateHandler(date) {
    return new Date(date).toLocaleString();
}

function toUpperCase(str) {
    return str.toUpperCase();
}

function component(item, name) {
    nameLower = name.toLowerCase();
    return item[nameLower];
}

