﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SMECService.Models;
using Microsoft.AspNetCore.Identity;

namespace SMECService.Data
{
    //public class CEMSContext : DbContext 

    public class CEMSContext : IdentityDbContext<IdentityUser>
    {
        public CEMSContext(DbContextOptions<CEMSContext> options) : base(options)
        {
        }

        public DbSet<Analyzer> Analyzers { get; set; }
        public DbSet<Focus> Focus { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<MeasuringComponent> MeasuringComponents { get; set; }
        public DbSet<Sensor> Sensors { get; set; }
        public DbSet<CurrentAnalogData> CurrentAnalogData { get; set; }
        public DbSet<HistoricalAnalogData> HistoricalAnalogData { get; set; }
        public DbSet<CalibrationFunction> CalibrationFunctions { get; set; }
        public DbSet<Periferic> Periferics { get; set; }
        public DbSet<SensorPeriferic> SensorPeriferics { get; set; }
        public DbSet<VLE> VLE { get; set; }
        public DbSet<IC> IC { get; set; }
        public DbSet<O2Ref> O2Ref { get; set; }
        public DbSet<Formula> Formulas { get; set; }
        public DbSet<Corrections> Corrections { get; set; }
        public DbSet<PerifericFormula> PerifericFormulas { get; set; }

        public DbSet<Serie> Series { get; set; }
        public DbSet<Chart> Chart { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<HistoricalAnalogData>()
                .HasKey(c => new { c.Id, c.TimeStamp });

            modelBuilder.Entity<Formula>()
                .HasKey(f => f.FormulaId);

            modelBuilder.Entity<VLE>()
                .HasKey(v => v.VLEId);

            modelBuilder.Entity<IC>()
                .HasKey(ic => ic.ICId);

            modelBuilder.Entity<PerifericFormula>()
            .HasKey(pf => pf.PerifericId);

            /* Corrections */
            modelBuilder.Entity<Corrections>()
            .HasKey(c => c.CorrectionId);

            modelBuilder.Entity<Corrections>()
                .HasOne(pf => pf.Sensor)
                .WithMany(p => p.Corrections)
                .HasForeignKey(c => c.SensorId);

            modelBuilder.Entity<Corrections>()
                .HasOne(pf => pf.PerifericFormulas)
                .WithMany(f => f.Corrections)
                .HasForeignKey(c => c.PerifericId);

            modelBuilder.Entity<Sensor>()
                .HasOne<Periferic>(s => s.Periferic)
                .WithOne(p => p.Sensor)
                .HasForeignKey<Periferic>(p => p.SensorId);

            modelBuilder.Entity<Serie>()
            .HasOne<Chart>(c => c.Chart)
            .WithMany(s => s.Series)
            .HasForeignKey(s => s.ChartId);


        }
    }
}
