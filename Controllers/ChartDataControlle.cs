using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using SMECService.Models;
using SMECService.Data;
using SMECService.Errors;

namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class ChartDataController: Controller
    {
        private readonly CEMSContext _context;

        public ChartDataController(CEMSContext context)
        {
            _context = context;
        }

        [HttpPost("{start_date:datetime}/{end_date:datetime}", Name = "GetChartData")]
        public IActionResult AmChartData([FromBody] Serie[] series, DateTime start_date, DateTime end_date)
        {

            var result = RequestChartData(series, start_date, end_date);
            if (result.Count == 0)
            {
                return NotFound(new NotFoundError("No se han encontrado datos entre " + start_date.ToString("dd/MM/yyyy") + " y " + end_date.ToString("dd/MM/yyyy")));
            }

            return new ObjectResult(result);
        }

        public List<Dictionary<string, object>> UtilData(Serie[] series, DateTime start_date, DateTime end_date)
        {
            var result = RequestChartData(series, start_date, end_date);

            return result;
        }

        private List<Dictionary<string, object>> RequestChartData(Serie[] series, DateTime start_date, DateTime end_date)
        {
            List<ChartDataRecord> items_results = new List<ChartDataRecord>();

            List<Dictionary<string, object>> _series = new List<Dictionary<string, object>>();
            List<string> elementos = new List<string>();
            Dictionary<string, object> foo;

            foreach (var serie in series)
            {
                elementos.Add(serie.Name);
                var _results = ChartDataRecords(serie.SensorId, serie.Name, start_date, end_date);
                items_results.AddRange(_results);
            }

            if (items_results.Count == 0)
            {
                return _series;
            }

            var group_results = items_results
            .GroupBy(x => x.date)
            .Where(g => g.Count() > 1)
            .Select(y => new ChartDictionary()
            {
                date = y.Key,
                records = y.ToDictionary(x => x.graph, x => x.value)
            }).ToList();

            foreach (var result in group_results)
            {
                foo = new Dictionary<string, object>();
                foo.Add("date", result.date);

                foreach (var record in result.records)
                {
                    foo.Add(record.Key.ToLower(), record.Value);
                }

                _series.Add(foo);
            }

            return _series;
        }

        private List<ChartDataRecord> ChartDataRecords(long id, string name, DateTime start_date, DateTime end_date)
        {
            bool gap_restart = true;
            ChartDataRecord lastItem = null;
            List<ChartDataRecord> items_wogaps = new List<ChartDataRecord>();


            var results = _context.HistoricalAnalogData
                  .AsNoTracking()
                  .Where(a => a.Id == id && a.TimeStamp >= start_date && a.TimeStamp <= end_date)
                  .Select(p =>
                  new ChartDataRecord()
                  {
                      date = p.TimeStamp,
                      graph = name,
                      value = p.Value
                  });
            foreach (ChartDataRecord item in results)
            {

                while (start_date < item.date)
                {
                    gap_restart = true;

                    if (lastItem != null)
                        items_wogaps.Add(new ChartDataRecord() { date = start_date, graph = lastItem.graph, value = lastItem.value });
                    else
                        items_wogaps.Add(new ChartDataRecord() { date = start_date, graph = item.graph, value = null });
                    start_date = start_date.AddMinutes(1);
                }
                if (gap_restart)
                {
                    gap_restart = false;
                    if (lastItem != null)
                        items_wogaps.Add(new ChartDataRecord() { date = start_date, graph = lastItem.graph, value = lastItem.value });
                    else
                        items_wogaps.Add(new ChartDataRecord() { date = start_date, graph = item.graph, value = null });
                }
                else
                {
                    items_wogaps.Add(item);
                    lastItem = item;
                }
                start_date = start_date.AddMinutes(1);

            }

            // while (start_date < end_date)
            // {
            //     items_wogaps.Add(new ChartDataRecord() { date = start_date, graph = null, value = null });
            //     start_date = start_date.AddMinutes(1);
            // }

            return items_wogaps;
        }


    }
}