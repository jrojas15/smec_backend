using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using SMECService.Models;
using SMECService.Data;
using SMECService.Errors;

namespace SMECService.Controllers
{
    [Route("api/[controller]")]

    public class ChartController : Controller
    {
        private readonly CEMSContext _context;

        public ChartController(CEMSContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
           // var item = (dynamic)null;

            var item = _context.Chart
                    .Include(c => c.Series)
                  .ToList();

            if (item == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado ningún gráfico"));
            }

            return new ObjectResult(item);
        }

        [HttpGet("{id}")]
        public IActionResult GetChartById(long id)
        {
            var item = _context.Chart
            .Include(c => c.Series)
            .ThenInclude(s => s.Sensor.Unit)
            .FirstOrDefault(c => c.ChartId == id);

            if (item == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado ningún gráfico"));
            }

            return new ObjectResult(item);


        }

        [HttpPost("Create")]
        public IActionResult Create([FromBody] Chart item)
        {
            if (item == null)
            {
                return BadRequest(new BadRequestError("Solicitud incorrecta"));
            }

            if (ItemExists(item.ChartId))
            {
                return BadRequest(new BadRequestError(item.Description + " ya existe"));
            }
            _context.Chart.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetPeriferic", new { id = item.ChartId }, item);

        }

        private bool ItemExists(int id)
        {
            return _context.Chart.Any(c => c.ChartId == id);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var item = _context.Chart.FirstOrDefault(c => c.ChartId == id);
            if (item == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado ningún gráfico"));
            }

            var series = _context.Series.Where(s => s.ChartId == id);
            if (series != null)
            {
                foreach (Serie serie in series)
                {
                    _context.Series.Remove(serie);
                }
            }

            _context.Chart.Remove(item);
            _context.SaveChanges();
            return Ok();

        }

    }

}