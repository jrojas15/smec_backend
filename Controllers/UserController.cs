﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Transactions;

using SMECService.Data;
using SMECService.ViewModels;
using SMECService.Models;
using SMECService.Errors;

using System;

using System.Security.Claims;
using Microsoft.AspNetCore.Http;



namespace SMECService.Controllers
{

    [Route("api/[controller]")]
    public class UserController : Controller
    {
        #region Constructor
        private readonly CEMSContext _context;
        private RoleManager<IdentityRole> _roleManager;

        private IHttpContextAccessor _httpContextAccessor;

        public UserController(
            RoleManager<IdentityRole> roleManager,
            UserManager<IdentityUser> userManager,
            ILogger<UserController> logger,
            IConfiguration configuration,
            CEMSContext context,
            IHttpContextAccessor httpContextAccessor
            )
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _logger = logger;
            _configuration = configuration;
            _context = context;
            _httpContextAccessor = httpContextAccessor;

        }
        #endregion

        [HttpGet("List")]
        public IActionResult GetUserList()
        {

            var UserRoles = _context.UserRoles.ToList();

            var userList = (from user in _context.Users
                            select new
                            {
                                UserId = user.Id,
                                Username = user.UserName,
                                RoleNames = (from userRole in UserRoles
                                             join Role in _context.Roles
                                             on userRole.RoleId equals Role.Id
                                             where userRole.UserId == user.Id
                                             select Role.Name).ToList()
                            });


            var userListVm = userList.Select(u => new User
            {
                UserId = u.UserId,
                UserName = u.Username,
                RoleNames = u.RoleNames
            });

            if (userListVm == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado ningún usuario"));
            }
            return new ObjectResult(userListVm);
        }


        [HttpGet("{name}")]
        public IActionResult GetUserByName(string name)
        {
            var UserRoles = _context.UserRoles.ToList();

            var _user = (from user in _context.Users
                         where user.UserName == name
                         select new User()
                         {
                             UserId = user.Id,
                             UserName = user.UserName,
                             RoleNames = (from userRole in UserRoles
                                          join Role in _context.Roles
                                          on userRole.RoleId equals Role.Id
                                          where userRole.UserId == user.Id
                                          select Role.Name).ToList()
                         });

            if (_user == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado ningún usuario"));
            }
            return new ObjectResult(_user);
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody]UserCreateViewModel model)
        {
            if (model == null) return new StatusCodeResult(500);
            if (await _roleManager.RoleExistsAsync(model.roleName))
                if (await _userManager.FindByNameAsync(model.name) == null)
                {
                    var user = new IdentityUser { SecurityStamp = System.Guid.NewGuid().ToString(), UserName = model.name, Email = model.email };
                    var result = await _userManager.CreateAsync(user, model.confirmedPassword);

                    if (result.Succeeded)
                    {
                        result = await _userManager.AddToRoleAsync(user, model.roleName);
                        return new StatusCodeResult(201);
                    }

                    else
                        return new StatusCodeResult(500);
                }
            return new StatusCodeResult(403);
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPost("update")]
        public async Task<IActionResult> UpdateUserName([FromBody] UpdateUserName model)
        {
            var user = await _userManager.FindByNameAsync(model.currentUserName);
            if (user == null)
            {
                return NotFound();
            }

            user.UserName = model.newUserName;
            await _userManager.UpdateNormalizedUserNameAsync(user);
            await _context.SaveChangesAsync();
            return new StatusCodeResult(201);
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador, Usuario")]
        [HttpPost("update/password/{Name}")]
        public async Task<IActionResult> UpdatePassword(string Name, [FromBody] UpdatePassword model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByNameAsync(Name);
            if (user == null)
            {
                return NotFound();
            }
            if (userId != user.Id)
            {
                return new UnauthorizedResult();
            }

            var updatePassWord = await _userManager.ChangePasswordAsync(user, model.currentPassword, model.newPassword);

            if (updatePassWord.Succeeded)
            {
                return new StatusCodeResult(201);
            }
            return new StatusCodeResult(400);

        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPost("reset/password/{Name}")]
        public async Task<IActionResult> ResetPassword(string Name, [FromBody] UpdatePassword model)
        {

            var user = await _userManager.FindByNameAsync(Name);
            if (user == null)
            {
                return NotFound();
            }


            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var resetPassword = await _userManager.ResetPasswordAsync(user, token, model.newPassword);

            if (resetPassword.Succeeded)
            {
                return new StatusCodeResult(201);
            }
            return new BadRequestResult();

        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpDelete("delete/{Name}")]
        public async Task<IActionResult> DeleteUser(string Name)
        {

            if (await _userManager.FindByNameAsync(Name) != null)
            {
                var user = await _userManager.FindByNameAsync(Name);
                var delete = await _userManager.DeleteAsync(user);
                return new StatusCodeResult(201);
            }
            else
            {
                return new BadRequestResult();
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPost("role/add")]
        public async Task<IActionResult> AddToRole([FromBody] AddUserRole model)
        {
            if (await _roleManager.RoleExistsAsync(model.Role))
                if (await _userManager.FindByNameAsync(model.Name) != null)
                {
                    var user = await _userManager.FindByNameAsync(model.Name);
                    var role = await _userManager.AddToRoleAsync(user, model.Role);
                    return new StatusCodeResult(201);
                }

            return new BadRequestResult();

        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpDelete("role/delete/{Name}/{Role}")]
        public async Task<IActionResult> DeleteFromRole(string Name, string Role)
        {
            if (await _roleManager.RoleExistsAsync(Role))
                if (await _userManager.FindByNameAsync(Name) != null)
                {
                    var user = await _userManager.FindByNameAsync(Name);
                    var role = await _userManager.RemoveFromRoleAsync(user, Role);
                    return new StatusCodeResult(201);
                }
                else
                {
                    return new StatusCodeResult(500);
                }
            return new StatusCodeResult(500);

        }

        #region Properties
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        #endregion
    }
}
