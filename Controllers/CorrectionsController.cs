using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SMECService.Models;
using SMECService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class CorrectionsController : Controller
    {
        private readonly CEMSContext _context;
        public CorrectionsController(CEMSContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Corrections> GetAll()
        {
            return _context.Corrections
            .Include(c => c.Sensor)
            .Include(c => c.PerifericFormulas)
            .ToList();
        }

        [HttpGet("{id}/sensor", Name = "GetCorrections")]
        public IActionResult GetById(long id)
        {
            var item = _context.Corrections
            .Where(c => c.SensorId == id)
            .Select(c => new CorrectionsDTO()
             {
                 CorrectionId = c.CorrectionId,
                 SensorId = c.SensorId,
                 name = c.PerifericFormulas.Periferic.Sensor.MeasuringComponent.Name,
                 PerifericId = c.PerifericId,
                 Formula = c.PerifericFormulas.Formula
             });

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost("Create")]
        public IActionResult Create([FromBody] Corrections Item)
        {
            if (Item == null)
            {
                return BadRequest();
            }
            _context.Corrections.Add(Item);
            _context.SaveChanges();
            return CreatedAtRoute("GetCorrections", new { id = Item.SensorId }, Item);
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(long id)
        {
            var item = _context.Corrections.FirstOrDefault(t => t.SensorId == id);
            if (item == null)
            {
                return NotFound();
            }

            _context.Corrections.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}