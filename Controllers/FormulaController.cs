using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SMECService.Models;
using SMECService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using SMECService.Errors;

namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class FormulaController : Controller
    {
        private readonly CEMSContext _context;

        public FormulaController(CEMSContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Formula> GetAll()
        {
            return _context.Formulas
                   .ToList();
        }

        [HttpPost("Create")]
        public IActionResult Create([FromBody] PerifericFormula item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            if (ItemExists(item.PerifericId))
            {
                return BadRequest(new BadRequestError("El periferico ya tiene asignado una fórmula"));
            }
            _context.PerifericFormulas.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute(new { id = item.PerifericId }, item);
        }

        [HttpPatch("update/{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] PerifericFormula item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.PerifericId)
            {
                return BadRequest(new BadRequestError("El periférico no coincide"));
            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound(new NotFoundError("No se ha encontrado ningún periférico"));
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        private bool ItemExists(int id)
        {
            return _context.PerifericFormulas.Any(c => c.PerifericId == id);
        }

    }
}