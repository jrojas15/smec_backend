﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using jsreport.Types;
using jsreport.AspNetCore;

using SMECService.Models;
using SMECService.Data;
using SMECService.Errors;
using SMECService.Controllers;



namespace Netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        public IJsReportMVCService JsReportMVCService { get; }

        private readonly CEMSContext _context;

        public ReportController(IJsReportMVCService jsReportMVCService, CEMSContext context)
        {
            JsReportMVCService = jsReportMVCService;
            _context = context;
        }

        [HttpPost("csvReport")]
        public async Task<IActionResult> CsvReport([FromBody] DataModel Data)
        {
            var result = new ChartDataController(_context).UtilData(Data.series, Data.start_date, Data.end_date);
            
            try
            {
            await ReportGenerator(Data.series, result, Data.fileName);
            return new ObjectResult(Data);
            }
            catch (Exception err)
            {
                Console.WriteLine();
                Console.WriteLine("+-----------------------------------+");
                Console.WriteLine(err.Message);
                Console.WriteLine("+-----------------------------------+");
                return BadRequest(new BadRequestError("|No se ha podido generar el reporte"));
            }
            finally
            {
                Console.WriteLine();
                Console.WriteLine("|     reporting finished      |");
            }
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        private async Task ReportGenerator(Serie[] series, List<Dictionary<string, object>> content, string fileName)
        {
            Console.WriteLine();
            Console.WriteLine("+-----------------------------------+");
            Console.WriteLine("|        Writting xlsx file         |");
            Console.WriteLine("+-----------------------------------+");
            var report = await JsReportMVCService.RenderByNameAsync("custom-template", new { header = series, body = content });

            using (var file = System.IO.File.Create($"wwwroot/invoice/{fileName}.xlsx"))
            {
                report.Content.CopyTo(file);
            }
        }
    }
}
