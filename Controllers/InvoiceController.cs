using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using jsreport.Types;
using jsreport.AspNetCore;

using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

using SMECService.Models;
using SMECService.Data;
using SMECService.Hubs;
using SMECService.Errors;


namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class InvoiceController : Controller
    {
        public IJsReportMVCService JsReportMVCService { get; }

        private readonly CEMSContext _context;

        public InvoiceController(IJsReportMVCService jsReportMVCService, CEMSContext context)
        {
            JsReportMVCService = jsReportMVCService;
            _context = context;
        }


        [HttpGet("generate-file")]
        public async Task<IActionResult> getPdf(string name)
        {
            var csv = await invoiceCsv("pepito_grillo2");
            Console.WriteLine("csv is ready");
            var pdf = await customInvoicePdf("gepetto");
            Console.WriteLine("pdf is ready");

            return new OkResult();
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> invoiceCsv(string name)
        {
            var id = 1;
            var start_date = new DateTime(2018, 11, 26);
            var end_date = new DateTime(2018, 11, 27);

            Console.WriteLine("Fetching current analog data ...");

            var items = _context.HistoricalAnalogData
                     .Where(a => a.Id == id && a.TimeStamp >= start_date && a.TimeStamp <= end_date)
                     .Select(p =>
                         new AnalogDataDTO()
                         {
                             TimeStamp = p.TimeStamp,
                             Value = p.Value,
                             StatusCode = 1,
                             Samples = p.Samples
                         }).ToList();

            if (items == null)
            {
                return NotFound();
            }

            Console.WriteLine("Initializing local jsreport for xlsx ...");
            var csv = await JsReportMVCService.RenderByNameAsync("custom-template", new { data = items });

            using (var file = System.IO.File.Create($"wwwroot/invoice/{name}.xlsx"))
            {
                csv.Content.CopyTo(file);
            }

            return new OkResult();
        }

        public async Task<IActionResult> customInvoicePdf(string name)
        {

            Console.WriteLine("Initializing local jsreport for pdf ...");


            var report = await JsReportMVCService.RenderByNameAsync("Invoice", InvoiceData);
            using (var file = System.IO.File.Create($"wwwroot/invoice/{name}.pdf"))
            {
                report.Content.CopyTo(file);
            }

            return new OkResult();
        }

        private static RenderRequest CustomRenderRequest = new RenderRequest()
        {
            Template = new Template()
            {
                Content = "Helo world from {{message}}",
                Engine = Engine.Handlebars,
                Recipe = Recipe.ChromePdf
            },
            Data = new
            {
                message = "jsreport for .NET!!!"
            }
        };

        static object ManubrioData = new
        {
            title = "Fight Fire With Fire"
        };

        static object InvoiceData = new
        {
            title = "SMECView",
            number = "123",
            seller = new
            {
                name = "Next Step Webs, Inc.",
                road = "12345 Sunny Road",
                country = "Sunnyville, TX 12345",
                time = new DateTime().ToString()
            },
            buyer = new
            {
                name = "Acme Corp.",
                road = "16 Johnson Road",
                country = "Paris, France 8060"
            },
            items = new[]
            {
                new { name = "Website design", price = 300 }
            }
        };
    }
}