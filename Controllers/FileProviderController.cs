using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.Extensions.FileProviders;

using SMECService.Models;
using SMECService.Data;
using SMECService.Hubs;
using SMECService.Errors;

namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class FileProviderController : Controller
    {
        private readonly IFileProvider _fileProvider;

        public FileProviderController(IFileProvider fileProvider)
        {
            _fileProvider = fileProvider;
        }

        public IDirectoryContents DirectoryContents { get; private set; }

        [HttpGet("invoice")]
        public IActionResult OnGet()
        {
            DirectoryContents = _fileProvider.GetDirectoryContents("wwwroot/invoice/");

            var result = DirectoryContents.Select(dir => new InvoiceModel()
            {
                Name = dir.Name,
                LastModified = dir.LastModified,
                Type = GetContentType(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/invoice", dir.Name))
            });

            return new ObjectResult(result);
        }


        [HttpGet("download/{name}")]
        public IActionResult downloadFileByName(string name)
        {
            string filePath = "wwwroot/invoice";
            string fileName = name;
            string file = filePath + "/" + fileName;

            var path = Path.Combine(Directory.GetCurrentDirectory(), filePath, fileName);

            // $"attachment; filename=\"{fileName}\""
            Response.Headers.Add("Content-Disposition", $"attachment; filename=\"{fileName}\"");
            // Response.Headers.Add("X-Content-Type-Options", "nosniff");

            return File(System.IO.File.ReadAllBytes(file), GetContentType(path));

        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

    }
}
