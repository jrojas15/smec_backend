﻿using Microsoft.AspNetCore.Mvc;
using SMECService.Errors;

namespace Netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ErrorMockController : ControllerBase
    {

        [HttpGet("notFound")]
        public IActionResult SendNotFoundeError()
        {
            return NotFound(new NotFoundError("No se ha encontrado ningún foco .Net custom error message"));
        }

        [HttpGet("badRequest")]
        public IActionResult SendBadRequestError()
        {
            return BadRequest(new BadRequestError("BadRequestError .Net"));
        }

        [HttpGet("unauthorized")]
        public IActionResult SendUnauthorizedError()
        {
            return Unauthorized();
        }

        [HttpGet("serverError")]
        public IActionResult SendServerSideError()
        {
            return StatusCode(500);
        }


    }
}
