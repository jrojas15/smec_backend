using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using SMECService.Models;
using SMECService.Data;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class SerieController : Controller
    {
        private readonly CEMSContext _context;

        private UserManager<IdentityUser> _userManager;

        private readonly IHttpContextAccessor _httpContextAccessor;


        public SerieController(CEMSContext context, UserManager<IdentityUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;

        }


        [HttpGet]
        public IEnumerable<Serie> GetAll()
        {

            return _context.Series.ToList();
        }

        [HttpGet("{id}", Name = "GetConfig")]
        public IActionResult GetById(long id)
        {
            var item = _context.Series.FirstOrDefault(t => t.SerieId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPost]
        public IActionResult Create([FromBody] Serie[] series)
        {
            if (series == null)
            {
                return BadRequest();
            }

            foreach (Serie serie in series)
            {
                _context.Series.Add(serie);
                _context.SaveChanges();
                // return CreatedAtRoute("GetConfig", new { id = serie.SerieId }, serie);
            }

            return Ok();

        }

        //delete
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            Console.WriteLine("===> Administrator");
            var claims = User.Claims;
            foreach (var claim in claims)
            {
                Console.WriteLine("CLAIIIMS");
                Console.WriteLine(claim);
            }
            var item = _context.Series.FirstOrDefault(s => s.SerieId == id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Series.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPatch("{id}")]

        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Serie item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.SerieId)
            {
                return BadRequest();

            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }
            return NoContent();
        }
        private bool ItemExists(int id)
        {
            return _context.Series.Any(s => s.SerieId == id);
        }

    }


}
