using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SMECService.Models;
using SMECService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;



namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class PerifericController : Controller
    {
        private readonly CEMSContext _context;

        public PerifericController(CEMSContext context)
        {
            _context = context;
        }

        // Get CAlibrationFunctions

        [HttpGet]
        public IActionResult GetAll()
        {
            var item = _context.Periferics
             .Include(p => p.Sensor)
                 .ThenInclude(s => s.MeasuringComponent)
             .Include(p => p.PerifericFormula)
                 .ThenInclude(p => p.Formula)
             .Select(p => new
             {
                 p.PerifericId,
                 p.SensorId,
                 name = p.Sensor.MeasuringComponent.Name,
                 PerifericFormula = p.PerifericFormula.Formula
             }).ToList();

            return new ObjectResult(item);
        }

        //Get by sensorId
        [HttpGet("{id}", Name = "GetPeriferic")]
        public IActionResult GetById(long id)
        {
            var item = _context.Periferics
             .Include(p => p.Sensor)
                 .ThenInclude(s => s.MeasuringComponent)
             .Include(p => p.PerifericFormula)
                 .ThenInclude(p => p.Formula)
             .Select(p => new
             {
                 p.PerifericId,
                 p.SensorId,
                 name = p.Sensor.MeasuringComponent.Name,
                 PerifericFormula = p.PerifericFormula.Formula
             }).FirstOrDefault(p => p.PerifericId == id);

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpGet("{id}/NoPeriferics")]
        public IActionResult GetNoPeriferics(long id)
        {
            var item = _context.Sensors
                .Include(s => s.MeasuringComponent)
                .Include(p => p.Periferic)
                    .Where(s => s.Periferic == null && s.AnalyzerId == id)
                    .Select(s => new
                    {
                        s.SensorId,
                        s.MeasuringComponent.Name
                    });

            return new ObjectResult(item);
        }

        [HttpGet("{id}/Periferics")]
        public IActionResult GetNPeriferics(long id)
        {
            var item = _context.Sensors
                .Include(s => s.MeasuringComponent)
                .Include(p => p.Periferic)
                    .ThenInclude(p => p.PerifericFormula)
                    .Where(s => s.Periferic != null && s.AnalyzerId == id)
                    .Select(s => new
                    {
                        s.Periferic.PerifericId,
                        s.SensorId,
                        s.MeasuringComponent.Name,
                        PerifericFormula = s.Periferic.PerifericFormula.Formula
                    });

            return new ObjectResult(item);
        }

        [HttpGet("{id}/sensor", Name = "GetPerifericBySensorId")]
        public IActionResult GetPerifericBySensorId(long id)
        {
            var item = _context.Periferics
             .Include(p => p.Sensor)
                 .ThenInclude(s => s.MeasuringComponent)
             .Include(p => p.PerifericFormula)
                 .ThenInclude(p => p.Formula)
             .Select(p => new
             {
                 p.PerifericId,
                 p.SensorId,
                 name = p.Sensor.MeasuringComponent.Name,
                 PerifericFormula = p.PerifericFormula
             }).FirstOrDefault(p => p.SensorId == id);

            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }


        //POST : api/periferic/create
        [HttpPost("Create")]
        public IActionResult Create([FromBody] Periferic item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            _context.Periferics.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetPeriferic", new { id = item.PerifericId }, item);

        }

        //DELETE : api/periferic/1
        [HttpDelete("delete/{id}")]
        public IActionResult Delete(long id)
        {
            var item = _context.Periferics.FirstOrDefault(p => p.PerifericId == id);
            if (item == null)
            {
                return NotFound();
            }
            /*  var correction = _context.Corrections.FirstOrDefault(c => c.PerifericId == id);
              if (correction != null)
              {
                  _context.Corrections.Remove(correction);
              }*/

            var perifericFormula = _context.PerifericFormulas.FirstOrDefault(pf => pf.PerifericId == id);
            if (perifericFormula != null)
            {
                _context.PerifericFormulas.Remove(perifericFormula);
            }

            _context.Periferics.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();
        }



        //UPDATE : api/calibrationfunction/1
        [HttpPatch("{id}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Periferic item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.PerifericId)
            {
                return BadRequest();
            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool ItemExists(int id)
        {
            return _context.Periferics.Any(c => c.PerifericId == id);
        }

        /** PerifericFormula */
        [HttpGet("formula")]
        public IEnumerable<PerifericFormula> getAllFormulas()
        {
            return _context.PerifericFormulas
            .Include(pf => pf.Periferic)
            .Include(pf => pf.Formula)
            .ToList();
        }

        [HttpGet("{id}/formula")]
        public IActionResult getFormulaById(long id)
        {
            var item = _context.PerifericFormulas
            .Select(p => new
            {
                p.PerifericId,
                p.Formula
            })
            .FirstOrDefault(pf => pf.PerifericId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

    }


}