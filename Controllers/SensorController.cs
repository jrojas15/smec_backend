﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

using SMECService.Models;
using SMECService.Data;
using SMECService.Hubs;
using SMECService.Errors;


using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

//  For more information on enabling MVC for empty projects, visit https:go.microsoft.com/fwlink/?LinkID=397860

namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class SensorController : Controller
    {
        private readonly CEMSContext _context;

        public SensorController(CEMSContext context)
        {
            _context = context;

        }

        [HttpGet]
        public IEnumerable<Sensor> GetAll()
        {
            return _context.Sensors
                .Include(s => s.Analyzer)
                .Include(s => s.MeasuringComponent)
                .Include(s => s.Unit)
                .Include(s => s.CalibrationFunctions)
                .Include(p => p.Periferic)
                .Include(c => c.Corrections)
                .ToList();
        }

        [HttpGet("{id}", Name = "GetSensor")]
        public IActionResult GetById(long id)
        {
            var item = _context.Sensors
                .Include(s => s.Analyzer)
                .Include(s => s.MeasuringComponent)
                .Include(s => s.Unit)
                .Include(s => s.CalibrationFunctions)
                .Include(p => p.Periferic)
                .Include(c => c.Corrections)
                    .ThenInclude(pf => pf.PerifericFormulas)
                        .ThenInclude(f => f.Formula)
                .FirstOrDefault(s => s.SensorId == id);
            if (item == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado ningún sensor"));
            }
            return new ObjectResult(item);
        }

        [HttpGet("analyzer/{id}", Name = "GetSensorsByAnalyzerId")]
        public IActionResult GetByAnalyzerId(long id)
        {
            var item = _context.Sensors
                           .Include(s => s.Analyzer)
                           .Include(s => s.MeasuringComponent)
                           .Include(s => s.Unit)
                           .Include(s => s.CalibrationFunctions)
                           .Include(p => p.Periferic)
                                .Where(s => s.AnalyzerId == id)
                                .ToList();

            if (item == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado ningún sensor"));
            }
            return new ObjectResult(item);
        }

        //  Valor limite de emision
        [HttpGet("vle/{id}")]
        public IActionResult GetSensorVle(long id)
        {
            var item = _context.VLE.FirstOrDefault(v => v.SensorId == id);
            return new ObjectResult(item);
        }
        //  intervalos de confianza
        [HttpGet("ic/{id}")]
        public IActionResult GetSensorIC(long id)
        {
            var item = _context.IC.FirstOrDefault(i => i.SensorId == id);
            return new ObjectResult(item);
        }

        /* API QUERY SAMPLE 
         * http:localhost:63389/api/Sensor/1/CurrentAnalogData
         * 404 is returned if date format in server locale is wrong
         */
        [HttpGet("{id}/CurrentAnalogData", Name = "GetCurrentAnalogData")]
        public IActionResult GetCurrentAnalogData(long id)
        {
            var item = _context.CurrentAnalogData
                .FirstOrDefault(t => t.Id == id);

            var sensor = _context.Sensors
                .Include(s => s.MeasuringComponent)
                .Include(s => s.Unit)
                .FirstOrDefault(s => s.SensorId == id);

            if (item == null || sensor == null)
            {
                return NotFound(new NotFoundError("No se han encontrado datos"));
            }

            var vle = _context.VLE.FirstOrDefault(v => v.SensorId == id);
            var ic = _context.IC.FirstOrDefault(i => i.SensorId == id);
            var cf = _context.CalibrationFunctions.OrderByDescending(x => x.Date).FirstOrDefault(c => c.SensorId == id);
            var O2Ref = _context.O2Ref.FirstOrDefault(o => o.SensorId == id);

            // Console.WriteLine("fooking hell");
            // var _calibratedValue = getCalibratedValue(cf.Slope, cf.Intercept, item.Value);
            // var _correctedValue = getCorrectedValue(_calibratedValue, O2Ref.Value, getO2CurrentValue());
            // var _validatedValue = getValidatedValue(_correctedValue, vle.Value, ic.Value);

            var analogData = new Visualization
            {
                TimeStamp = item.TimeStamp,
                Value = item.Value,
                Samples = item.Samples,
                StatusCode = 1,
                Name = sensor.MeasuringComponent.Name,
                Unit = sensor.Unit.Name,
                CalibratedValue = 0,
                CorrectedValue = 0,
                ValidatedValue = 0,
                VLE = vle.Value,
                IC = ic.Value
            };
            return new ObjectResult(analogData);
        }

        private double getO2CurrentValue()
        {
            var O2 = _context.Sensors.FirstOrDefault(s => s.MeasuringComponent.Name == "O2");
            var O2Value = _context.CurrentAnalogData.FirstOrDefault(t => t.Id == O2.SensorId);
            return O2Value.Value;
        }

        /* API QUERY SAMPLE 
         * http:localhost:63389/api/Sensor/1/HistoricalAnalogData/10-26-2017/10-27-2017
         * 404 is returned if date format in server locale is wrong
         */
        [HttpGet("{id}/HistoricalAnalogData/{start_date:datetime}/{end_date:datetime}", Name = "GetHistoricalAnalogData")]
        public IActionResult GetHistoricalAnalogData(long id, DateTime start_date, DateTime end_date)
        {
            /* QueryObjects => DTO */

            var items = _context.HistoricalAnalogData
                    .Where(a => a.Id == id && a.TimeStamp >= start_date && a.TimeStamp <= end_date)
                    .Select(p =>
                        new AnalogDataDTO()
                        {
                            TimeStamp = p.TimeStamp,
                            Value = p.Value,
                            StatusCode = 1,
                            Samples = p.Samples
                        });

            if (items == null)
            {
                return NotFound();
            }

            List<AnalogDataDTO> items_wogaps = new List<AnalogDataDTO>();


            bool gap_restart = true;
            AnalogDataDTO lastItem = null;

            foreach (AnalogDataDTO item in items)
            {
                while (start_date < item.TimeStamp)
                {
                    gap_restart = true;

                    if (lastItem != null)
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = lastItem.Value, StatusCode = lastItem.StatusCode });
                    else
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = null, StatusCode = null });
                    start_date = start_date.AddMinutes(1);
                }
                if (gap_restart)
                {
                    gap_restart = false;
                    if (lastItem != null)
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = lastItem.Value, StatusCode = lastItem.StatusCode });
                    else
                        items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = null, StatusCode = null });
                }
                else
                {
                    items_wogaps.Add(item);
                    lastItem = item;
                }
                start_date = start_date.AddMinutes(1);
            }

            while (start_date < end_date)
            {
                items_wogaps.Add(new AnalogDataDTO() { TimeStamp = start_date, Value = null, StatusCode = null });
                start_date = start_date.AddMinutes(1);
            }

            return new ObjectResult(items_wogaps);
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPost]
        public IActionResult Create([FromBody] Sensor item)
        {
            if (item == null)
            {
                return BadRequest(new BadRequestError("Solicitud incorrecta"));
            }

            if (!ItemExists(item.SensorId))
            {
                return BadRequest(new BadRequestError("El sensor ya existe"));
            }

            _context.Sensors.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetSensor", new { id = item.SensorId }, item);
        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var item = _context.Sensors.FirstOrDefault(t => t.SensorId == id);
            if (item == null)
            {
                return NotFound();
            }
            _context.Sensors.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();

        }

        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPatch("{id}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] Sensor item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.SensorId)
            {
                return BadRequest(new BadRequestError("El sensor no coincide"));
            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound(new NotFoundError("No se ha encontrado ningún sensor"));
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool ItemExists(int id)
        {
            return _context.Sensors.Any(c => c.SensorId == id);
        }

        public System.Nullable<double> getCalibratedValue(double slope, double intercept, double value)
        {
            Console.WriteLine("getCalibratedValue => " + slope + ", " + intercept + ", " + value);

            return intercept * value + intercept;

        }

        public System.Nullable<double> getCorrectedValue(System.Nullable<double> Calbiratedvalue, double O2Ref, double O2CurrentValue)
        {
            return Calbiratedvalue * (21 - O2Ref) / (21 - O2CurrentValue);
        }
        public System.Nullable<double> getValidatedValue(System.Nullable<double> correctedValue, double VLE, double IC)
        {

            if (correctedValue < VLE)
            {
                return correctedValue - (correctedValue * IC / 100);
            }
            else if (correctedValue >= VLE)
            {
                return correctedValue - (VLE * IC / 100);
            }
            else
            {
                return 0;
            }
        }

    }

}

