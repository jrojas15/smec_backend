﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

using SMECService.Models;
using SMECService.Data;
using SMECService.ViewModels;


namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private readonly CEMSContext _context;

        public RoleController(RoleManager<IdentityRole> roleManager,
            ILogger<RoleController> logger,
            IConfiguration configuration,
            CEMSContext context)
        {
            _roleManager = roleManager;
            _logger = logger;
            _configuration = configuration;
            _context = context;
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody]RoleRequestViewModel model)
        {
            if (model == null) return new StatusCodeResult(500);
            if (await _roleManager.RoleExistsAsync(model.name))
                return new StatusCodeResult(403);
            var result = await _roleManager.CreateAsync(new IdentityRole(model.name));
            if (result.Succeeded)

                return new StatusCodeResult(201);
            else
                return new StatusCodeResult(500);
        }

        [HttpGet("list")]
        public IEnumerable<Role> GetRoleList()
        {
            var roleList = _context.Roles.ToList();

            var roleListVm = roleList.Select(r => new Role
            {
                Name = r.Name
            });
            return roleListVm;
        }


        #region Properties
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        #endregion
    }
}
