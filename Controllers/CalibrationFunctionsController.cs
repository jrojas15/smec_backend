using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SMECService.Models;
using SMECService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using System.Collections;
using System.Globalization;
using SMECService.Errors;


namespace SMECService.Controllers
{
    [Route("api/[controller]")]
    public class CalibrationFunctionController : Controller
    {
        private readonly CEMSContext _context;

        public CalibrationFunctionController(CEMSContext context)
        {
            _context = context;
        }


        // Get CAlibrationFunctions
        [HttpGet]
        public IEnumerable<CalibrationFunction> GetAll()
        {
            return _context.CalibrationFunctions.OrderByDescending(cf => cf.Date)
                     .ToList();
        }

        // get calibrationFunction by AnalyzerId
        [HttpGet("analyzer/{id}")]
        public IActionResult GetcalibrationFnByAnalyzerId(long id)
        {
            var items = (from a in _context.Analyzers
                         join s in _context.Sensors on a.AnalyzerId equals s.AnalyzerId
                         where s.AnalyzerId == id
                         select new
                         {
                             Name = s.MeasuringComponent.Name,
                             SensorId = s.SensorId,
                             CalibrationFunction = s.CalibrationFunctions.OrderByDescending(x => x.Date).FirstOrDefault()

                         }).ToList();

            return new ObjectResult(items);
        }

        [HttpGet("sensor/{id}")]
        public IActionResult GetcalibrationFnBySensorId(long id)
        {
            // var items = (from s in _context.Sensors
            //              where s.SensorId == id
            //              select new
            //              {
            //                  CalibrationFunction = s.CalibrationFunctions.OrderByDescending(x => x.Date).FirstOrDefault()
            //              }).ToList();

            var cf = _context.CalibrationFunctions
            .Where(c => c.SensorId == id)
            .OrderByDescending(x => x.Date).ToList();

            return new ObjectResult(cf);
        }

        //Get by id
        [HttpGet("{id}", Name = "GetCalibrationFunction")]
        public IActionResult GetById(long id)
        {
            var item = _context.CalibrationFunctions
                .Include(c => c.Sensor)
                .FirstOrDefault(c => c.CalibrationFunctionId == id);
            if (item == null)
            {
                if (item == null)
                {
                    return NotFound(new NotFoundError("No se ha encontrado funciones de calibración"));

                }
            }
            return new ObjectResult(item);
        }

        //POST : api/calibrationfunction/create
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPost("Create")]
        public IActionResult Create([FromBody] CalibrationFunction item)
        {
            if (item == null)
            {
                return BadRequest(new BadRequestError("Solicitud incorrecta"));
            }

            if (ItemExists(item.CalibrationFunctionId))
            {
                return BadRequest(new BadRequestError("la función de calibración ya existe"));
            }
            _context.CalibrationFunctions.Add(item);
            _context.SaveChanges();
            return CreatedAtRoute("GetCalibrationFunction", new { id = item.CalibrationFunctionId }, item);

        }

        //DELETE : api/calibrationfunction/1
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var item = _context.CalibrationFunctions.FirstOrDefault(t => t.CalibrationFunctionId == id);
            if (item == null)
            {
                return NotFound(new NotFoundError("No se ha encontrado funciones de calibración"));
            }

            _context.CalibrationFunctions.Remove(item);
            _context.SaveChanges();
            return new NoContentResult();
        }

        //UPDATE : api/calibrationfunction/1
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "Administrador")]
        [HttpPatch("{id}")]
        public async Task<IActionResult> Update([FromRoute]int id, [FromBody] CalibrationFunction item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != item.CalibrationFunctionId)
            {
                return BadRequest();
            }
            _context.Entry(item).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool ItemExists(int id)
        {
            return _context.CalibrationFunctions.Any(c => c.CalibrationFunctionId == id);
        }
    }

}