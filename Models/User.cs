using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using SMECService.Models;

namespace SMECService.Models
{
    public class User
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public List<string> RoleNames { get; set; }

    }

    public class UpdateUserName
    {
        public string currentUserName { get; set; }
        public string newUserName { get; set; }

    }
}
