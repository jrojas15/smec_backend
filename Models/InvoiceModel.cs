using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    public class InvoiceModel
    {
        public string Name { get; set; }
        public DateTimeOffset LastModified { get; set; }
        public string Type { get; set; }
    }


    public class DataModel
    {
        //public string Name { get; set; }
        public string fileName { get; set; }
        public Serie[] series { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
    }
}