namespace SMECService.Models
{
    public class UpdatePassword
    {
        public string currentPassword { get; set; }
        public string newPassword { get; set; }

    }
}
