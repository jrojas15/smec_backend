using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("CalibrationFunction")]
    public class CalibrationFunction
    {
        public int CalibrationFunctionId { get; set; }
        public int SensorId { get; set; }
        [DataType(DataType.Date)]
        public DateTime? Date { get; set; }
        public double Slope { get; set; }
        public double Intercept { get; set; }
        public Sensor Sensor { get; set; }
        
    }
}
