using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SMECService.Models;

namespace SMECService.Models
{

    public class Role
    {
        public string Name { get; set; }
    }

    public class AddUserRole
    {
        public string Name { get; set; }
        public string Role { get; set; }
    }
}
