using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("VLE")]
    public class VLE
    {
        public int VLEId { get; set; }
        public int SensorId { get; set; }
        public int Value { get; set; }
    }
}