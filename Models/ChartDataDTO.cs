using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

namespace SMECService.Models
{
    public class ChartDictionary
    {
        public DateTime date { get; set; }
        public IDictionary<string, System.Nullable<double>> records { get; set; }
    }

    public class ChartDataRecord
    {
        public DateTime date { get; set; }
        public string graph { get; set; }
        public System.Nullable<double> value { get; set; }


    }
}
