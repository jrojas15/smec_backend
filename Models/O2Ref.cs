using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("O2ref")]
    public class O2Ref
    {
        public int Id { get; set; }
        public int SensorId { get; set; }
        public int Value { get; set; }
    }
}