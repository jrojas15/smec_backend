using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("Periferic")]
    public class Periferic
    {
        public int PerifericId { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; }
        public PerifericFormula PerifericFormula { get; set; }
    }

    [Table("PerifericFormula")]
    public class PerifericFormula
    {
        public int PerifericId { get; set; }
        public Periferic Periferic { get; set; }

        public int FormulaId { get; set; }
        public Formula Formula { get; set; }
        public ICollection<Corrections> Corrections { get; set; }

    }

    [Table("Formula")]
    public class Formula
    {
        public int FormulaId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public ICollection<PerifericFormula> PerifericFormulas { get; set; }

    }
}
