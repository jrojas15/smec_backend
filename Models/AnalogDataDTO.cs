﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SMECService.Models
{
    public class AnalogDataDTO
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public System.Nullable<double> Value { get; set; }
        public System.Nullable<int> StatusCode { get; set; }
        public System.Nullable<int> Samples { get; set; }


    }

    public class Visualization
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public System.Nullable<double> Value { get; set; }
        public System.Nullable<int> StatusCode { get; set; }
        public System.Nullable<int> Samples { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public System.Nullable<double> CalibratedValue { get; set; }
        public System.Nullable<double> CorrectedValue { get; set; }
        public System.Nullable<double> ValidatedValue { get; set; }
        public int VLE { get; set; }
        public int IC { get; set; }

    }

    public class Methods
    {

        public double getCalibratedValue(double slope, double intercept, double value)
        {
            return intercept * value + intercept;
        }

        public double getCorrectedValue(double Calbiratedvalue, double O2Ref, double O2CurrentValue)
        {
            return Calbiratedvalue * (21 - O2Ref) / (21 - O2CurrentValue);
        }
        public double getValidatedValue(double correctedValue, double VLE, double IC)
        {

            if (correctedValue < VLE)
            {
                return correctedValue - (correctedValue * IC / 100);
            }
            else if (correctedValue >= VLE)
            {
                return correctedValue - (VLE * IC / 100);
            }
            else
            {
                return 0;
            }
        }

    }

}
