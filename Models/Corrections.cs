using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{
    [Table("Corrections")]
    public class Corrections
    {
        public int CorrectionId { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; }

        public int PerifericId { get; set; }
        public PerifericFormula PerifericFormulas { get; set; }
    }

    public class CorrectionsDTO
    {
        public int CorrectionId { get; set; }

        public int SensorId { get; set; }
        public string name { get; set; }
        public int PerifericId { get; set; }
        public Formula Formula { get; set; }

    }
}