using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMECService.Models
{


    [Table("Serie")]
    public class Serie
    {
        public int SerieId { get; set; }
        public int SensorId { get; set; }
        public int ChartId { get; set; }
        public string Name { get; set; }
        public Chart Chart { get; set; }
        public Sensor Sensor { get; set; }
    }


    [Table("Chart")]
    public class Chart
    {
        public int ChartId { get; set; }
        public string Description { get; set; }
        public ICollection<Serie> Series { get; set; }

    }





}
