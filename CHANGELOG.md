## [4.1.3] - 28/01/2019
### Added
- Controlador para `CalibrationFunctions`.

## [4.1.2] - 07/01/2019
### Fixed
- Un usuario solo puede cambiar a si mismo la contraseña.
- El administrador puede resetear las contraseñas de los usuarios.

## [4.1.1] - 04/01/2019
### Added
- `SerieController`: añadido el método `GetSeriesByFocusId`.
- `UserController`: añadido el método `GetUserByName`.

## [4.1.0] - 20/12/2018

### Added
- Authorización por roles en los controladores.
- `UserController:' listar los usuarios con sus roles respectivos,.
- `UserController:' acutalizar nombre de usuario.
- `UserController:' actualizar la contraseña del usuario.
- `UserController:' añadir usuarios a roles.
- `UserController:' eliminar usuarios de roles.
- Pomelo para Mysql.

### Changed
`TokenController` añadida función GetValidClaims en la que se añade el rol del usuario validado.
- `UserController:' se puede crear un usuario añadiendolo a un rol.
