﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.IdentityModel.Tokens.Jwt;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;

using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.FileProviders;

using SMECService.Data;
using SMECService.Hubs;
using SMECService.Models;

using Newtonsoft.Json.Serialization;
using System.Security.Claims;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

using jsreport.AspNetCore;
using jsreport.Local;
using jsreport.Binary;
using jsreport.Types;
// using jsreport.Client;

namespace Netcore
{
    public class Startup
    {
        private IHostingEnvironment _env;
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;

        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            // Sql Connection
            services.AddDbContext<CEMSContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Mysql Connection
            /*  services.AddDbContext<CEMSContext>(options =>
             {
                 options.UseMySql(Configuration.GetConnectionString("DefaultConnection"),
                 mySqlOptions =>
                 {
                     mySqlOptions.ServerVersion(new Version(8, 0, 13), ServerType.MySql);
                 });
             });*/


            // Add Authentication with JWT Tokens
            services.AddAuthentication(opts =>
           {
               opts.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
               opts.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
               opts.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
           })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidIssuer = Configuration["Auth:Jwt:Issuer"],
                        ValidAudience = Configuration["Auth:Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            System.Text.Encoding.UTF8.GetBytes(Configuration["Auth:Jwt:Key"])),
                        ClockSkew = TimeSpan.Zero,
                        RequireExpirationTime = true,
                        ValidateIssuer = true,
                        ValidateIssuerSigningKey = true,
                        ValidateAudience = true,

                    };
                    cfg.IncludeErrorDetails = true;
                });


            services.AddIdentity<IdentityUser, IdentityRole>()
                        .AddRoles<IdentityRole>()
                        .AddRoleManager<RoleManager<IdentityRole>>()
                        .AddDefaultTokenProviders()
                        .AddEntityFrameworkStores<CEMSContext>();

            services.AddMvc()

            .AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // jsreport
            services.AddJsReport(new LocalReporting()
                    .RunInDirectory(Path.Combine(Directory.GetCurrentDirectory(), "jsreport"))
                    .KillRunningJsReportProcesses()
                    .UseBinary(JsReportBinary.GetBinary())
                    .Configure(cfg => cfg.AllowedLocalFilesAccess().FileSystemStore().BaseUrlAsWorkingDirectory())
                    .AsUtility()
                    .Create()
                    );
            // services.AddJsReport(new ReportingService("http://jsreport:5488"));

            services.AddCors(options => options.AddPolicy("CorsPolicy",
                   builder =>
                   {
                       builder.AllowAnyMethod()
                              .AllowAnyHeader()
                              .AllowAnyOrigin()
                             .AllowCredentials();
                   }));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            

            services.AddHttpsRedirection(options =>
          {
              options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
              options.HttpsPort = 443;
          });

            // fileProvider
            var physicalProvider = _env.ContentRootFileProvider;
            var manifestEmbeddedProvider =
                new ManifestEmbeddedFileProvider(Assembly.GetEntryAssembly());
            var compositeProvider =
                new CompositeFileProvider(physicalProvider, manifestEmbeddedProvider);

            services.AddSingleton<IFileProvider>(compositeProvider);



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();

            app.UseCors("CorsPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");

            });
        }
    }
}
